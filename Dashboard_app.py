# -*- coding: utf-8 -*-
"""
Created on Wed Jul 11 11:15:54 2018

@author: Thomas
"""
import dash
from dash.dependencies import Input, Output
from loremipsum import get_sentences
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
from loremipsum import get_sentences

app = dash.Dash()

app.scripts.config.serve_locally = True

vertical = True

#=======================Histogram of the dashboard============================
app.layout = html.Div([
   
    html.H1(children='Dashboard for eProcess Nigeria'),
        html.Div(
            dcc.Tabs(
                tabs=[
                    {'label': 'DASHBOARD', 'value': 3},
                    {'label': 'PAYMENTS', 'value': 1},
                    {'label': 'TRANSACTIONS', 'value': 2},
                    {'label': 'REPORTS', 'value':4},
                    {'label': 'SETTINGS', 'value': 5}
                ],
                value=3,
                id='tabs',
                vertical=vertical,
                style={
                    'height': '100vh',
                    'borderRight': 'thin lightgrey solid',
                    'textAlign': 'left'
                }
            ),
            style={'width': '20%', 'float': 'left'}
        ),
        html.Div(
            html.Div(id='tab-output'),
            style={'width': '80%', 'float': 'right'}
        )
    ], style={
        'fontFamily': 'Sans-Serif',
        'margin-left': 'auto',
        'margin-right': 'auto',
    })


@app.callback(Output('tab-output', 'children'), [Input('tabs', 'value')])
def display_content(value):
    data = [
        {
                'x': ['May 10', 'May 15', 'May 20', 'May 30','June 5','June 10',
                      'June 15','June 20','June 25','June 30'],
                'y': ['30', '10', '20', '30','70', '15', '20', '50','80','40'],
                
                'name': 'Profit',
                'marker': {
               
            },
            'type': ['bar','pie'][int(value) % 3]
        },

        
        {
            'x': ['May 10', 'May 15', 'May 20', 'May 30','June 5','June 10',
                'June 15','June 20','June 25','June 30'],
            'y': ['10', '25', '20', '50','40', '65', '40', '65','70','80'],

            'name': 'Sales',
            'marker': {
               
            },
            'type': ['bar','pie'][int(value) % 3]
        },

        {
            'x': ['May 10', 'May 15', 'May 20', 'May 30','June 5','June 10',
                  'June 15','June 20','June 25','June 30'],
            'y': ['50', '30', '10', '90','50', '45', '30', '20','30','30'],
                
            'name': 'Cash',
            'marker': {
             
            },
            'type': ['bar','pie'][int(value) % 3]
        },

        {
            'x': ['May 10', 'May 15', 'May 20', 'May 30','June 5','June 10',
                  'June 15','June 20','June 25','June 30'],
            'y': ['20', '30', '40', '50','40', '35', '60', '80','50','40'],
               
            'name': 'Balance',
            'marker': {
              
            },
            'type': ['bar','pie'][int(value) % 3]
        }
    ]

    return html.Div([
        dcc.Graph(
            id='graph',
            figure={
                'data': data,
                'layout': {
                    'margin': {
                        'l': 30,
                        'r': 0,
                        'b': 30,
                        't': 0
                    },
                    'legend': {'x': 0, 'y': 1}
                }
                
            }
        ),
        html.Div(' '.join(get_sentences(10)))
    ])



if __name__ == '__main__':
    app.run_server(debug=True)
    
